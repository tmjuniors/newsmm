<?php

use Illuminate\Database\Seeder;

class TaskStatusSeeder extends Seeder
{
  public function run()
  {
    DB::table('task_status')->truncate();
    DB::table('task_status')->insert([
      [
        'title' => 'Ожидает выполнения', //Должен иметь id = 1, см. TaskController@store
        'tag' => 'awaits',
      ],[
        'title' => 'В работе',
        'tag' => 'performing',
      ],[
        'title' => 'Выполнено',
        'tag' => 'done',
      ]
    ]);
  }
}
