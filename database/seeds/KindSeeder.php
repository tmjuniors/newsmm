<?php

use Illuminate\Database\Seeder;

class KindSeeder extends Seeder
{
  public function run()
  {
    DB::table('kind')->truncate();
    DB::table('kind')->insert([
      [
        // этот вид пользака должен быть обязательно первым (смотри AuthController@register)
        'title' => 'Заказчик',
        'tag' => 'customer',
      ],[
        'title' => 'SMM ВКонтакте',
        'tag' => 'vk',
      ],[
        'title' => 'SMM Instagramm',
        'tag' => 'inst',
      ],
    ]);
  }
}
