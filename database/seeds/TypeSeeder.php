<?php

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
  public function run()
  {
    DB::table('type')->truncate();
    DB::table('type')->insert([
      [
        'title' => 'Исполнитель',
        'tag' => 'worker',
      ],[
        'title' => 'Заказчик',
        'tag' => 'customer',
      ],[
        'title' => 'Администратор',
        'tag' => 'admin',
      ],
    ]);
  }
}
