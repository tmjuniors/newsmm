<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Таблица документов проекта
 */

class CreateDocumentTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('document', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title', 50);
      $table->text('description');
      $table->unsignedBigInteger('project_id');
      $table->unsignedBigInteger('author_id'); // user
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('document');
  }
}
