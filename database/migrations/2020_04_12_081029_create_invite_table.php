<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInviteTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('invite', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('email')->nullable();
      $table->unsignedBigInteger('project_id')->nullable();
      $table->unsignedBigInteger('inviter_id')->nullable();
      $table->unsignedBigInteger('user_id')->nullable();
      $table->string('token',100)->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('invite');
  }
}
