<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('task', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title', 100);
      $table->text('description');
      $table->unsignedBigInteger('creator_id'); // создатель задания
      $table->unsignedBigInteger('project_id'); // проект в котором находится это задание
      $table->unsignedTinyInteger('task_status_id'); // статус выполнения задачи
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('task');
  }
}
