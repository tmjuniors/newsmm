<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('file', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('title', 100);
      $table->string('src', 100);
      $table->unsignedBigInteger('author_id');
      $table->unsignedBigInteger('document_id')->nullable();
      $table->timestamps();
    });
}

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('file');
  }
}
