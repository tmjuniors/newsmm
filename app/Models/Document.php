<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
  protected $table = 'document';

  protected $fillable = [
    'title',
    'description',
    'project_id',
    'author_id',
  ];

  public static function create_rules()
  {
    return [
      'title'       => 'string|max:100',
      'description' => 'string',
      'project_id'  => 'integer',
      'author_id'   => 'integer',
    ];
  }

  public function files()
  {
    return $this->hasMany(File::class, 'document_id', 'id');
  }
}
