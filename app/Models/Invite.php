<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
  protected $table = 'invite';

  protected $fillable = [
    'firstname',
    'lastname',
    'user_id',
    'email',
    'token',
    'inviter_id',
    'project_id',
  ];

  public function project ()
  {
    return $this->belongsTo(Project::class, 'project_id', 'id');
  }
}
