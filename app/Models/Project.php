<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;

class Project extends Model
{
  use SoftDeletes;

  protected $table = 'project';

  protected $fillable = [
    'title',
    'description',
    'head_worker_id',
    'head_customer_id'
  ];

  public static function create_rules()
	{
		return [
      'title'            => 'required|string|max:50',
      'description'      => 'required|string',
      'head_worker_id'   => 'required|integer',
      'head_customer_id' => 'nullable|integer',
		];
  }

  public static function update_rules()
	{
		return [
      'title'            => 'required|string|max:50',
      'description'      => 'required|text',
      'head_worker_id'   => 'required|integer',
      'head_customer_id' => 'nullable|integer',
		];
  }

  public function users()
  {
    return $this->belongsToMany(User::class, 'project_users', 'project_id', 'user_id');
  }

  // public function setOtherUsersAttribute($user_id)
  // {
  //   $users = $this->belongsToMany(User::class, 'project_users', 'project_id', 'user_id');
  //   $this->attributes['other_users'] = $users->where('id', '!=', $user_id);
  // }

  public function tasks()
  {
    return $this->hasMany(Task::class, 'project_id', 'id');
  }

  public function documents()
  {
    return $this->hasMany(Document::class, 'project_id', 'id');
  }
}
