<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;

class Task extends Model
{
  use SoftDeletes;

  protected $table = 'task';

  protected $fillable = [
    'title',
    'description',
    'creator_id',
    'project_id',
    'task_status_id'
  ];

  public static function create_rules()
	{
		return [
      'title'          => 'required|string|max:100',
      'description'    => 'required|string',
      'creator_id'     => 'required|integer',
      'project_id'     => 'required|integer',
      'task_status_id' => 'required|integer',
		];
  }

  public static function update_rules()
	{
		return [
      'title'          => 'required|string|max:100',
      'description'    => 'required|string',
      'task_status_id' => 'required|integer',
		];
  }

  public function users()
  {
    return $this->belongsToMany(User::class, 'task_users', 'task_id', 'user_id');
  }
}
