<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;

class File extends Model
{
  protected $table = 'file';

  protected $fillable = [
    'document_id',
    'author_id',
    'title',
    'src',
  ];

  public function document()
  {
    return $this->belongsTo(User::class, 'document_id', 'id');
  }
}
