<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Models\Type;

class TypeController extends Controller
{
  protected function list()
  {
    $types = Type::where('tag', '!=', 'admin')->get();
    return compact('types');
  }
}
