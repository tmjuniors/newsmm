<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

use App\Models\Invite;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Project;

class InviteController extends Controller
{
  public function process (Request $request) {
    $user  = User::where('email', $request['user']['email'])->first();

    $token = Str::uuid()->toString();

    $invite = Invite::create([
      'user_id'    => $user->id,
      'project_id' => $request['project']['id'],
      'email'      => $request['user']['email'],
      'inviter_id' => $request['inviter']['id'],
      'token'      => $token,
    ]);

    return response()->json([], 200);
  }

  public function accept (Request $request) {
    $invite  = Invite::findOrFail($request['invite']['id']);
    $user    = User::findOrFail($invite['user_id']);
    $project = Project::findOrFail($invite->project_id);

    $project->users()->attach($user);

    $invite->delete();
    return response()->json([], 200);
  }
}
