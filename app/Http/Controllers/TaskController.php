<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\User;
use App\Models\Task;

class TaskController extends Controller
{
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $input = $request['task'];
    $input['creator_id'] = auth()->user()->id;
    $input['project_id'] = $request['project']['id'];
    $input['task_status_id'] = 1;

    $validator = Validator::make($input, Task::create_rules());
    if ($validator->fails())
      return response()->json(['errors' => $validator->errors()], 422);

    $task = Task::create($input);
    return compact('task');
  }

  /**
   * Update
   */
  public function update(Request $request)
  {
    $input = $request['task'];
    $task = Task::findOrFail($input['id']);

    $validator = Validator::make($input, Task::update_rules());
    if ($validator->fails())
      return response()->json(['errors' => $validator->errors()], 422);

    $task->update($input);
    return compact('task');
  }

  /**
   * Поместить в корзину
   */
  public function delete(Request $request)
  {
    $task = Task::findOrFail($request['task']['id']);
    $task->trashed();

    return response()->json([], 200);
  }

  /**
   * Полное удаление
   */
  public function destroy(Request $request)
  {
    $task = Task::findOrFail($request['task']['id']);
    $task->forceDelete();

    return response()->json([], 200);
  }

  /**
   * Прикрепить участника к заданию
   */
  public function attach(Request $request)
  {
    $task = Task::findOrFail($request['task']['id']);
    $user = User::findOrFail($request['user']['id']);

    $task->users()->attach($user->id);
    $task->load('users');
    return compact('task');
  }

  /**
   * Открепить участника от задания
   */
  public function detach(Request $request)
  {
    $task = Task::findOrFail($request['task']['id']);
    $user = User::findOrFail($request['user']['id']);

    $task->users()->detach($user->id);
    return compact('task');
  }

}
