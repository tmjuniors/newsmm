<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use App\Models\Invite;
use App\Models\Project;
use App\Models\File;
use App\Models\Document;
use App\User;

class DocumentController extends Controller
{
  public function store(Request $request)
  {
    $file     = File::findOrFail($request['file']['id']);
    $project  = Project::findOrFail($request['project']['id']);
    $document = $request['document'];

    $document['project_id'] = $project->id;
    $document['author_id'] = auth()->user()->id;

    $validator = Validator::make($document, Document::create_rules());
    if ($validator->fails())
      return response()->json(['errors' => $validator->erorrs()], 200);

    $document = Document::create($document);

    $file->update(['document_id' => $document->id]);

    return response()->json([], 200);

    // foreach ($request->file('file_list') as $file) {
    //   $file_real_name = $file->getClientOriginalName();
    //   $file_extension = $file->getClientOriginalExtension();
    //   $next_id = Picture::max('id') + 1;
    //   $file_hash = sha1($next_id . '_' . $input['id'] . $input['author_id'] . Carbon::now()).'.'.$file_extension;
    //   Picture::create([
    //     'location_id' => $input['id'],
    //     // 'author_id'   => Auth::user()->id,
    //     'author_id'   => $input['author_id'],
    //     'title'       => $file_real_name,
    //     'src'         => $file_hash,
    //   ]);
    //   Storage::putFileAs('public/location/'.$input['id'], $file, $file_hash, 'public');
    // }

    // Storage::put('documents/', $fileContents);
  }

  public function get(Request $request)
  {
    $document = Document::findOrFail($request['document']['id']);

    $file = File::findOrFail($document->files()->firstOrFail()->id);

    $url = env('APP_URL') . '/storage/public/document/' . $file->src;

    return compact('url');
  }
}
