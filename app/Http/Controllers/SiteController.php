<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

class SiteController extends Controller
{
  protected function home()
  {
    return view('home');
  }
}
