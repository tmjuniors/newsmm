<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
  /**
   * Возвращает пользователя со всеми возмодными связями и данными
   */
  public function get()
  {
    $user = auth()->user()->load(
      'type',
      'projects',
        'projects.users',
        'projects.tasks',
          'projects.tasks.users',
        'projects.documents',
      'invites',
        'invites.project',
    );
    return compact('user');
  }
}
