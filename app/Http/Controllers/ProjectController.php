<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\User;
use JWTAuth;

class ProjectController extends Controller
{
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $user  = auth()->user();
    $input = $request['project'];
    $input['head_worker_id'] = $user->id;

    $validator = Validator::make($input, Project::create_rules());
    if ($validator->fails())
      return response()->json(['errors' => $validator->errors()], 422);

    $project = Project::create($input);
    $user->projects()->attach($project);

    return compact('project');
  }

  /**
   * Update
   */
  public function update(Request $request)
  {
    $input = $request['project'];
    $project = Project::findOrFail($input['id']);

    $validator = Validator::make($input, Project::update_rules());
    if ($validator->fails())
      return response()->json(['errors' => $validator->errors()], 422);

    $project->update($input);
    return compact('project');
  }

  /**
   * Поместить в корзину
   */
  public function delete(Request $request)
  {
    $project = Project::findOrFail($request['project']['id']);
    $project->trashed();

    return response()->json([], 200);
  }

  /**
   * Полное удаление
   */
  public function destroy(Request $request)
  {
    $project = Project::findOrFail($request['project']['id']);
    $project->forceDelete();

    return response()->json([], 200);
  }

  /**
   * Метод для смены главного заказчика проекта
   */
  public function changeHeadCustomer(Request $request)
  {
    $project = Project::findOrFail($request['project']['id']);
    $project->head_customer_id = $request['user']['id'];

    return response()->json([], 200);
  }

  /**
   * Выгнать участника
   */
  public function kick(Request $request)
  {
    $project = Project::findOrFail($request['project']['id']);
    $users   = $request['users'];
    $project->users()->detach($users);

    return response()->json([], 200);
  }

}
