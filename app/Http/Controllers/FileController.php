<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use App\Models\Invite;
use App\Models\Project;
use App\Models\File;
use App\User;

class FileController extends Controller
{
  public function store(Request $request) {
    $file = $request['file'];

    $file_real_name = $file->getClientOriginalName();
    $file_extension = $file->getClientOriginalExtension();
    $next_id = File::max('id') + 1;
    $file_hash = sha1($next_id . '_' . Carbon::now() . '_' . date('Y-m-d')) . '.' . $file_extension;

    Storage::putFileAs('public/document/', $file, $file_hash, 'public');

    $file = File::create([
      'author_id'   => auth()->user()->id,
      'title'       => $file_real_name,
      'src'         => $file_hash,
    ]);

    return compact('file');

    // foreach ($request->file('file_list') as $file) {
    //   $file_real_name = $file->getClientOriginalName();
    //   $file_extension = $file->getClientOriginalExtension();
    //   $next_id = Picture::max('id') + 1;
    //   $file_hash = sha1($next_id . '_' . $input['id'] . $input['author_id'] . Carbon::now()).'.'.$file_extension;
    //   Picture::create([
    //     'location_id' => $input['id'],
    //     // 'author_id'   => Auth::user()->id,
    //     'author_id'   => $input['author_id'],
    //     'title'       => $file_real_name,
    //     'src'         => $file_hash,
    //   ]);
    //   Storage::putFileAs('public/location/'.$input['id'], $file, $file_hash, 'public');
    // }

    // Storage::put('documents/', $fileContents);
  }
}
