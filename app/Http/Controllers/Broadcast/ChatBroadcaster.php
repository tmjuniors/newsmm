<?php

namespace App\Http\Controllers\Broadcast;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Events\Message as BMessage;
use App\User;
use App\Models\Type;

class ChatBroadcaster extends Controller
{
  public function messages(Request $request)
  {
    BMessage::dispatch($request['message']);
    return response()->json(['success send'], 200);
  }
}
