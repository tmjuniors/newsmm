<?php

namespace App\Http\Controllers\Broadcast;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Events\User as BUser;
use App\Models\Type;
use App\User;

class UserBroadcaster extends Controller
{
  public function messages(Request $request)
  {
    $user = auth()->user()->load(
      'type',
      'projects',
        'projects.users',
        'projects.tasks',
          'projects.tasks.users',
        'projects.documents',
      'invites',
        'invites.project',
    );
    BUser::dispatch(collect(compact('user'))->values()[0]);
    return response()->json([], 200);
  }
}
