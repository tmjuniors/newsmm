<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

use App\Models\Type;
use App\Models\Project;
use App\Models\Invite;

class User extends Authenticatable implements JWTSubject
{
  use Notifiable;

  public static function signup_rules() {
    return [
      'email'     => 'string|required|unique:users,email|max:60',
      'password'  => 'string|required|min:6',
      'firstname' => 'string|required|max:30',
      'lastname'  => 'string|required|max:30',
    ];
  }

  protected $fillable = [
    'firstname',
    'lastname',
    'email',
    'password',
    'type_id',
    'kind_id',
  ];

  protected $hidden = [
    'password', 'remember_token',
  ];

  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  public function getJWTIdentifier()
  {
    return $this->getKey();
  }

  public function getJWTCustomClaims()
  {
    return [];
  }


  public function setPasswordAttribute($password)
  {
    if ( !empty($password) ) {
      $this->attributes['password'] = Hash::make($password);
    }
  }

  public function type()
  {
    return $this->belongsTo(Type::class, 'type_id', 'id');
  }

  public function projects()
  {
    return $this->belongsToMany(Project::class, 'project_users', 'user_id', 'project_id');
  }

  public function invites()
  {
    return $this->hasMany(Invite::class, 'user_id', 'id');
  }
}
