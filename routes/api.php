<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('auth')->group(function(){
  Route::group(['middleware' => 'api'], function ($router) {
    Route::post('signin',  'AuthController@signin');
    Route::post('signout', 'AuthController@signout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me',      'AuthController@me');
  });
  Route::post('/signup',      'AuthController@signup');
});

Route::prefix('user')->group(function(){
  Route::get('/', 'UserController@get');
});

Route::prefix('task')->group(function(){
  Route::post('/', 'TaskController@store');
  Route::put('/',  'TaskController@update');
  Route::put('/user/attach', 'TaskController@attach');
  Route::put('/user/detach', 'TaskController@detach');
});

Route::prefix('document')->group(function(){
  Route::post('/', 'DocumentController@store');
  Route::post('/get',  'DocumentController@get');
});

Route::prefix('file')->group(function(){
  Route::post('/',       'FileController@store');
});

Route::prefix('invite')->group(function(){
  Route::post('/',       'InviteController@process');
  Route::post('/accept', 'InviteController@accept');
});

Route::prefix('project')->group(function(){
  Route::post('/', 'ProjectController@store');
  Route::post('/kick', 'ProjectController@kick');
});

Route::prefix('type')->group(function(){
  Route::get('/list', 'TypeController@list');
});

Route::post('/token', 'AuthController@token');

Route::post('/upload', function(){
  return response()->json([], 200);
});


