const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .js('resources/js/main.js', 'public/app/js')
  .sass('resources/sass/app.scss', 'public/app/css')
  .webpackConfig({
    resolve: {
      extensions: ['.js', '.json', '.vue'],
      alias: {
        '~': path.join(__dirname, './resources/js'),
        'components': path.resolve(__dirname, './resources/js/components')
      }
    },
    output: {
      chunkFilename: 'app/js/[name].[chunkhash].js',
    }
  })

if (mix.inProduction()) {
  mix.version();
  /**
   * Возможно когда то понадобиться.
   */
  // mix.webpackConfig({
  //   plugins: [
  //     new BundleAnalyzerPlugin(),
  //     new webpack.DefinePlugin({
  //         APP_NAME: JSON.stringify(process.env.APP_NAME),
  //         APP_URL: JSON.stringify(process.env.APP_URL),
  //     })
  //   ]
  // });
}
