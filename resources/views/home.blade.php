<!DOCTYPE HTML>
<html lang="ru">
<head>
  <meta charset="utf-8">
  {{-- <title>{{ config('app.service_name', 'Laravel') }} - {{ config('app.name', 'Laravel') }}</title> --}}
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <script>
    window.app_config = {
      service_name: '{{ config('app.service_name', 'Laravel') }}',
      name: '{{ config('app.name', 'Laravel') }}',
      comet_key: '{{ config('app.comet_key', '123') }}',
      version: '{{ config('app.version', "1.0.1") }}',
    };
  </script>

  <link rel="shortcut icon" href="/img/favicon.png?v={{ config('app.version') }}">
  <!-- Favicons -->
  <link rel="icon" type="image/png" href="/img/favicon/favicon-196x196.png?v={{ config('app.version') }}" sizes="196x196" />
  <link rel="icon" type="image/png" href="/img/favicon/favicon-96x96.png?v={{ config('app.version') }}" sizes="96x96" />
  <link rel="icon" type="image/png" href="/img/favicon/favicon-32x32.png?v={{ config('app.version') }}" sizes="32x32" />
  <link rel="icon" type="image/png" href="/img/favicon/favicon-16x16.png?v={{ config('app.version') }}" sizes="16x16" />
  <link rel="icon" type="image/png" href="/img/favicon/favicon-128.png?v={{ config('app.version') }}" sizes="128x128" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <meta name="msapplication-TileColor" content="#FFFFFF" />

	<link href="{{ asset('css/bootstrap.min.css') }}?v={{ config('app.version') }}" rel="stylesheet">
  <link href="{{ asset('css/styles.css') }}?v={{ config('app.version') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  {{-- <script src="https://comet-server.com/CometServerApi.js"></script> --}}

</head>

<body id="body">
  <div id="app"></div>

  <script src="{{ mix('app/js/main.js') }}?v={{ config('app.version') }}"></script>
  <link href="{{ mix('app/css/app.css') }}?v={{ config('app.version') }}" rel="stylesheet">
</body>

</html>
