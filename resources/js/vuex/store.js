import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export const store = new Vuex.Store({
  plugins: [createPersistedState()],

  state: {
    access_token: null,
    user: null,
  },

  actions: {

    signin({commit, dispatch}, user) {
      return new Promise((resolve, reject) => {
        axios.post('/api/auth/signin', {
          email: user.email,
          password: user.password
        })
        .then((r) => {
          let access_token = r.data.access_token;
          commit('set_access_token', access_token);
          resolve();
        })
        .catch((err) => {
          reject(err);
        });
      });
    },

    signout({commit}) {
      return new Promise((resolve, reject) => {
        axios.post('/api/auth/signout')
        .then((r) => {
          commit('remove_access_token');
          commit('remove_user');
          resolve();
        })
        .catch((err) => {
          reject(err);
        });
      });
    },

    checkAccessToken({commit}){
      return new Promise((resolve, reject) => {
        axios.post('/api/token').then((r) => {
          if (r.data.token === 'dead') {
            commit('remove_access_token');
            commit('remove_user');
            resolve(false);
          }
          else
            resolve(true);
        })
        .catch((err) => {
          reject(err);
        });
      });
    },

    updateUser() {
      return new Promise ((resolve, reject) => {
        axios.post('/broadcast/user').then(() => {
          resolve();
        })
        .catch((err) => {
          reject(err)
        });
      });
    },

    userRequest({commit}, access_token) {
      return new Promise((resolve, reject) => {
        axios.get('/api/user', {
          headers: {
            'Authorization' : 'Bearer ' + access_token
          }
        })
        .then((r) => {
          let user = r.data.user;
          commit('set_user', user);
          resolve();
        })
        .catch((err) => {
          reject(err);
        });
      });
    }
  },

  mutations: {
    set_access_token (state, access_token) {
      state.access_token = access_token;
    },
    remove_access_token: state => {
      state.access_token = null;
    },
    set_user (state, user) {
      state.user = user;
    },
    remove_user: state => {
      state.user = null;
    },
    setRouteLoading(state, val) {
      state.routeLoading = val;
    },
  },

  getters: {
    isAuthenticated: state => {
      if (state.access_token)
        return true;
      return false;
    },
    access_token: state => {
      return state.access_token;
    },
    user: state => {
      return state.user;
    },
  }

});
