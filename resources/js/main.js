require('./bootstrap');

import Vue from 'vue';
import FishUI from 'fish-ui';
import DatePicker from 'vue2-datepicker';
import { router } from './router/routes';
import { store } from './vuex/store';
import { sync } from 'vuex-router-sync';
import App from './App';

Vue.use(FishUI);
Vue.use(DatePicker);

router.beforeEach((to, from, next)=>{
  store.commit('setRouteLoading', true);
  next();
});

router.afterEach((to, from)=>{
  store.commit('setRouteLoading', false);
});

sync(store, router);

const VueApp = new Vue({
  router,
  store,
  el: '#app',
  template: '<App/>',
  components: { App },
  created() {
    if (this.$store.getters.isAuthenticated) {
      let that = this;
      that.$store.dispatch('updateUser');
      window.Echo.channel('user').listen('User', ({user}) => {
        that.$store.commit('set_user', user)
      });
    }
    else {
      this.$router.push('/signin');
    }

    this.$router.beforeEach((to, from, next) => {
      if (to.path !== '/signin' && !this.$store.getters.isAuthenticated) {
        this.$router.push('/signin');
      }
      next();
    });
  },
});

export default VueApp;
