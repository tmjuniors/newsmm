import Vue from 'vue';
import VueRouter from 'vue-router'
import {store} from '~/vuex/store'
import axios from 'axios'

Vue.use(VueRouter);

axios.interceptors.request.use(
  config => {
    const access_token = store.getters.access_token;
    if (store.getters.isAuthenticated) config.headers.Authorization = `Bearer ${access_token}`;
    return config;
  },
  error => Promise.reject(error)
);

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next();
    return
  }
  next('/dashboard')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
    return
  }
  next('/')
}

const Home       = () => import(/* webpackChunkName: "Home" */ '../views/Home.vue');
const ClientHome = () => import(/* webpackChunkName: "ClientHome" */ '../views/client/Index');
const Signin     = () => import(/* webpackChunkName: "Signin" */ '../views/auth/Signin.vue');
const Signup     = () => import(/* webpackChunkName: "Signup" */ '../views/auth/Signup.vue');

// User profile
// const Profile = () => import(/* webpackChunkName: "Profile" */ '../views/profile/Index.vue');
const Bell      = () => import(/* webpackChunkName: "Bell" */ '../views/Bell.vue');
const Dashboard = () => import(/* webpackChunkName: "ClientDashboard" */ '../views/Dashboard');
const Tarif     = () => import(/* webpackChunkName: "ClientDashboard" */ '../views/Tarif');

export const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/client/dashboard',
    children: [
      // {
      //   path: 'profile',
      //   name: 'profile',
      //   component: Profile,
      //   beforeEnter: ifAuthenticated,
      // },

      {
        path: 'client',
        component: ClientHome,
        beforeEnter: ifAuthenticated,
        children: [
          {
            path: 'bell',
            name: 'bell',
            component: Bell,
          }, {
            path: 'dashboard',
            name: 'client-dashboard',
            component: Dashboard,
          }, {
            path: 'tarif',
            name: 'tarif',
            component: Tarif,
          },
        ]
      },

    ],
  }, {
    path: '/signin',
    name: 'signin',
    component: Signin,
    beforeEnter: ifNotAuthenticated,
  }, {
    path: '/signup',
    name: 'signup',
    component: Signup,
    beforeEnter: ifNotAuthenticated,
  }, {
    path: '*',
    redirect: '/',
  },
];

export const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'active',
  routes
});
